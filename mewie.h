#ifndef MEWIE_H
#define MEWIE_H

#define _XOPEN_SOURCE 700

#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

extern const char *mewie_name;
extern const char *mewie_version;
extern const char *mewie_usage;

extern FILE *mewie_current_in_file;
extern char *mewie_current_in_file_name;

extern size_t mewie_current_line;
extern size_t mewie_current_column;

void mewie_die(char *);

void mewie_parse_args(int *, char ***);

FILE *mewie_open_in_file(int *, char ***);

int mewie_getc(void);

enum NyashcaTokenType { // (Preferred value)
	MEWIE_TOKEN_LINE_BREAK, // N/A
	MEWIE_TOKEN_EXPRESSION_END, // N/A
	MEWIE_TOKEN_INLINE_EXPRESSION_START, // N/A
	MEWIE_TOKEN_INLINE_EXPRESSION_END, // N/A
	MEWIE_TOKEN_QUOTE, // N/A
	MEWIE_TOKEN_INT_VALUE, // as_int
	MEWIE_TOKEN_FLOAT_VALUE, // as_float
	MEWIE_TOKEN_STRING_VALUE, // as_string
	MEWIE_TOKEN_SYMBOL_VALUE, // as_string
	MEWIE_TOKEN_FUNCTION_START, // N/A
	MEWIE_TOKEN_BRANCH_START, // N/A
	MEWIE_TOKEN_BRANCH_AGAIN, // N/A
	MEWIE_TOKEN_BRANCH_OTHERWISE, // N/A
	MEWIE_TOKEN_LOOP_START, // N/A
	MEWIE_TOKEN_BLOCK_END // N/A
};

struct NyashcaString {
	size_t length; // Excluding null terminator
	
	char *value; // Null-terminated
};

union NyashcaTokenValue {
	int64_t as_int;
	double as_float;
	struct NyashcaString as_string;
};

struct NyashcaToken {
	enum NyashcaTokenType type;
	
	union NyashcaTokenValue value;
};

struct NyashcaToken *mewie_get_token(void);

void mewie_print_token(struct NyashcaToken *);

enum NyashcaNodeType { // (Preferred value)
	MEWIE_NODE_EXPRESSION, // as_node_list
	MEWIE_NODE_INT_VALUE, // as_int
	MEWIE_NODE_FLOAT_VALUE, // as_float
	MEWIE_NODE_STRING_VALUE, // as_string
	MEWIE_NODE_VARIABLE_REFERENCE, // as_string
	MEWIE_NODE_SYMBOL_VALUE, // as_string
	MEWIE_NODE_LIST_VALUE, // as_node_list
	MEWIE_NODE_FUNCTION_VALUE, // as_node_list
	MEWIE_NODE_BRANCH, // as_node_list
	MEWIE_NODE_LOOP // as_node_list
};

struct NyashcaNodeList {
	struct NyashcaNode *value;
	
	struct NyashcaNodeList *next;
};

union NyashcaNodeValue {
	struct NyashcaNodeList *as_node_list;
	int64_t as_int;
	double as_float;
	struct NyashcaString as_string;
};

struct NyashcaNode {
	enum NyashcaNodeType type;
	
	union NyashcaNodeValue value;
};

struct NyashcaNode *mewie_get_node(void);

void mewie_print_node(struct NyashcaNode *, size_t);

void mewie_run_ast(void);

#endif
